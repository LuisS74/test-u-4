package cl.ufro.dci.qrservice.controller;

import cl.ufro.dci.qrservice.helpers.QRGenerator;
import cl.ufro.dci.qrservice.service.UsuarioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;
import java.util.logging.Logger;

@RestController
@RequestMapping("/qrcode")
public class QRController {
    private final UsuarioService usuarioService;

    public QRController(UsuarioService usuarioService){
        this.usuarioService  = usuarioService;
    }

    @PostMapping(value = "/generate", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<BufferedImage> generateQRCode(@RequestBody String textMsg) throws Exception {
        textMsg = "";
        textMsg = usuarioService.mensaje();
        logger.info("response: " + textMsg);
        return okResponse(QRGenerator.generateQRCodeImage(textMsg));
    }

    private ResponseEntity<BufferedImage> okResponse(BufferedImage image) {
        return new ResponseEntity<>(image, HttpStatus.OK);
    }


    private static final Logger logger = Logger.getLogger(QRController.class.getName());
}