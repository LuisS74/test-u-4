package cl.ufro.dci.qrservice.model;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Usuario {
    private String rut;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombre;
    private String email;
    private Date fechaNacimiento;
    private String comuna;

    public Usuario() {
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public boolean validarRut(String rut) {
        boolean validacion = false;

        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }

            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }
        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }

        return validacion;
    }

    public boolean validarApellidoPaterno(String apellidoPaterno) {
        String regex = ".*\\d.*";

        if(apellidoPaterno.length()>30 || Pattern.compile(regex).matcher(apellidoPaterno).matches()){
            return false;
        }else{
            return true;
        }
    }

    public boolean validarApellidoMaterno(String apellidoMaterno) {
        String regex = ".*\\d.*";

        if(apellidoMaterno.length()>30 || Pattern.compile(regex).matcher(apellidoMaterno).matches()){
            return false;
        }else{
            return true;
        }
    }

    public boolean validarNombre(String nombre) {
        String regex = ".*\\d.*";

        if(nombre.length()>30 || Pattern.compile(regex).matcher(nombre).matches()){
            return false;
        }else{
            return true;
        }
    }

    public boolean validarEmail(String email) {
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(email);

        if (mather.find() == true) {
           return true;
        } else {
           return false;
        }
    }
}
