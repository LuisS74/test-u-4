package cl.ufro.dci.qrservice.service;

import cl.ufro.dci.qrservice.model.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    public ResponseEntity<String> crearQR(Usuario usuario) {
        if (!usuario.validarNombre(usuario.getNombre())) {
            return ResponseEntity.badRequest().body("El nombre es incorrecto");
        } else if (!usuario.validarNombre(usuario.getApellidoPaterno())) {
            return ResponseEntity.badRequest().body("El apellido paterno es incorrecto");
        } else if (!usuario.validarNombre(usuario.getApellidoMaterno())) {
            return ResponseEntity.badRequest().body("El apellido materno es incorrecto");
        } else if (usuario.getRut() != null && !usuario.validarRut(usuario.getRut())) {
            return ResponseEntity.badRequest().body("Rut invalido");
        } else if (!usuario.validarEmail(usuario.getEmail())) {
            return ResponseEntity.badRequest().body("El email es invalido");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body("Error al generar QR!");
    }

    public String mensaje(){
        Usuario usuario = new Usuario();
        crearQR(usuario);
        return usuario.getRut() + "," + usuario.getApellidoPaterno() + "," + usuario.getApellidoMaterno() + "," + usuario.getNombre() + "," + usuario.getEmail();
    }
}
