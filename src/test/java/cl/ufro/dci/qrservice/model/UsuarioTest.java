package cl.ufro.dci.qrservice.model;

import org.junit.jupiter.api.*;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class UsuarioTest {
    Usuario usuario;

    @BeforeEach
    void setUp() {
        usuario = new Usuario();
    }

    @Test
    @DisplayName("Test rut de usuario")
    public void testRut(){
        usuario.setRut("21.158.172-7");
        Assertions.assertTrue(usuario.validarRut(usuario.getRut()));
    }

    @Test
    @DisplayName("Test apellido paterno")
    public void testValidarApellidoPaterno() {
        usuario.setApellidoPaterno("Sandoval");
        Assertions.assertTrue(usuario.validarApellidoPaterno(usuario.getApellidoPaterno()));
    }

    @Test
    @DisplayName("Test apellido materno")
    public void testValidarApellidoMaterno() {
        usuario.setApellidoMaterno("Pino");
        Assertions.assertTrue(usuario.validarApellidoMaterno(usuario.getApellidoMaterno()));
    }

    @Test
    @DisplayName("Test nombre")
    public void testValidarNombre() {
        usuario.setNombre("John");
        Assertions.assertTrue(usuario.validarNombre(usuario.getNombre()));
    }

    @Test
    @DisplayName("Test email")
    public void testEmail() {
        usuario.setEmail("John12@gmail.com");
        Assertions.assertTrue(usuario.validarEmail(usuario.getEmail()));
    }

    /*
        @Test
    @DisplayName("Test mayor de edad")
    public void testEdad() {
        usuario.setFechaNacimiento(new Date(12-02-2000));
        Assertions.assertTrue(usuario.validarEdad(usuario.getFechaNacimiento()));
    }

    @Test
    @DisplayName("Test comuna")
    public void testComuna() {
        usuario.setComuna("Antofagasta");
        Assertions.assertTrue(usuario.validarComuna(usuario.getComuna()));
    }

    @AfterEach
    void tearDown() {
    }*/
}